class Time(object):
    def __init__(self, hour=0, minute=0):
        self.hour = hour
        self.minute = minute

    # clasa instantei obietului este data ca primul parametru in mod implicit
    @classmethod
    def from_string(cls, time_str):
        hour, minute = map(int, time_str.split(':'))
        return cls(hour, minute)

    # nici self (instanta obiectului) nici cls (clasa) este data implicit
    @staticmethod
    def is_valid(time_str):
        hour, minute = map(int, time_str.split(':'))
        return hour <= 23 and minute <= 59

    def display(self):
        print(Time.from_string('10:15'))
        print(Time.is_valid('10:15'))
        time = Time()
        print(time.is_valid('15:10'))


def from_string(time_str):
    hour, minute = map(int, time_str.split(':'))
    return (hour, minute)


def is_valid(time_str):
    hour, minute = map(int, time_str.split(':'))
    return hour <= 23 and minute <= 59


def display():
    print(from_string('10:15'))
    print(is_valid('10:15'))


if __name__ == '__main__':
    result = Time.is_valid('10:59')
    print(result)
    clock = Time.from_string('10:59')
    print(clock)
    t = Time(23, 15)
    t.display()
    print(t.from_string)
    print(t.is_valid)

    print(from_string)
    print(from_string('10:15'))
    print(is_valid)
    print(is_valid('10:15'))
    print(display())
