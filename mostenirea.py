# mostenirea stabileste o relatie ierarhica intre clase
# - reduce redundanta codului
# - creste viteza de dezvoltare


class Animal:
    def talk(self):
        print('A talking animal')

    def move(self):
        print('Moving')


class Person(Animal):  # clasa Person mosteneste de la clasa Animal
    def talk(self):
        """ Suprascrierea metodei talk mostenita de la clasa Animal """
        print('A talking person')


print('Instantierea obiectului animal_obj de tipul Animal')
animal_obj = Animal()
animal_obj.talk()
animal_obj.move()

print('Instantierea obiectului person_obj de tipul Person')
person_obj = Person()
person_obj.talk()
