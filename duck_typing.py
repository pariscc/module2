# duck typing-ul ajuta la apelarea dinamica a metodelor / functiilor
# - foarte folosit in Python
# - mareste eficienta si reduce complexitatea


class Duck:
    def quack(self):
        print('Quack, quack!')


class Person:
    def quack(self):
        print('I\'m Quacking!')


# functie
def in_the_forest(mallard):
    mallard.quack()


print('Instantiem doua obiecte d si p de tipul Duck respectiv Person')
d = Duck()
p = Person()

print('Dam ca argument unei funtii obiectul de tipul Duck')
in_the_forest(d)
print('Dam ca argument unei funtii obiectul de tipul Person')
in_the_forest(p)
