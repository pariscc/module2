# plimorfismul este relevant in special in limbajele de programare statice;
# ajuta la interactiunea si manipularea datelor indiferent de tipul sau
# structura acestora.


class Document:
    def __init__(self, name):
        self.name = name

    def show(self):
        raise NotImplementedError('Must implement abstract method')


class Pdf(Document):
    def show(self):
        return 'Show pdf contents!'


class Word(Document):
    def show(self):
        return 'Show word contents!'


print('Implementing a list of objects')
documents = [Pdf('Document1'), Pdf('Document2'), Word('Document3')]

for document in documents:
    print(document.name + ': ' + document.show())
