# incapsularea da capacitatea de a ascunde partile dintr-un obiect nerelevante
# pentru utilizator
# - da o interfata
# - creste securitatea


# implementare POO folosind abstractizarea si incapsularea
class Bag(object):
    def __init__(self):
        self.items = []

    def add_item(self, item):
        self.items.append(item)

    def remove_item(self, item):
        self.items.remove(item)

    def list_bag(self):
        for item in self.items:
            print(item)


# implementare procedurala
BAG_ITEMS = []


def add_item(item):
    global BAG_ITEMS  # cu global accesam o var din namespace-ul superior
    BAG_ITEMS.append(item)


def remove_item(item):
    global BAG_ITEMS
    BAG_ITEMS.remove(item)


def list_bag():
    for item in BAG_ITEMS:
        print(item)


# test
if __name__ == '__main__':
    print('*** POO ***')
    print('Instantierea b = Bag()')
    b = Bag()
    print(b)
    print('Apelam metoda add_item')
    b.add_item('item1')
    b.add_item('item2')
    print('Apelam metoda list_bag')
    b.list_bag()
    print('Apelam metoda remove_item')
    b.remove_item('item1')
    b.list_bag()

    print('*** Procedural ***')
    print('Apelam functia add_item')
    add_item('item1')
    add_item('item2')
    print('Apelam functia list_bag')
    list_bag()
    print('Apelam functia remove_item')
    remove_item('item2')
    list_bag()
