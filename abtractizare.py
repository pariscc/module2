# abstractizare duce la o dezvoltare mai rapida a programelor
# - reduce complexitatea
# - creste eficienta


class Bag(object):
    # atribute (stocheaza 'calitati' ale obiectului)
    number_of_items = 0

    # contructor
    def __init__(self):
        pass

    # metode (actiuni asupra obiectului)
    def add_item(self, item):
        pass

    def remove_item(self, item):
        pass

    def list_bag(self):
        pass
